﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarpunLv1
{
    class Zabiljeska
    {
        private String tekst;
        private String autor;
        private int RazinaVaznosti;

        public void setTekst(String Tekst) { this.tekst = Tekst; }
        private void setAutor(String Autor) { this.autor = Autor; }
        public void setRazinaVaznosti(int Razina) { this.RazinaVaznosti = Razina; }
        public String getTekst() { return this.tekst; }
        public String getAutor() { return this.autor; }
        public int getRazinaVaznosti() { return this.RazinaVaznosti; }

        public Zabiljeska(String Tekst, String Autor, int Razina)
        {
            this.tekst = Tekst;
            this.autor = Autor;
            this.RazinaVaznosti = Razina;
        }

        public Zabiljeska()
        {
            this.tekst = "BleBleBle.";
            this.autor = "Josip Ćošković";
            this.RazinaVaznosti = 2;
        }

        public Zabiljeska(Zabiljeska Novela)
        {
            tekst = Novela.tekst;
            autor = Novela.autor;
            RazinaVaznosti = Novela.RazinaVaznosti;
        }

        public String Tekst
        {
            get { return this.tekst; }
            set { this.tekst = value; }
        }
        public String Autor
        {
            get { return this.autor; }
            set { this.autor = value; }
        }
        public int Razina
        {
            get { return this.RazinaVaznosti; }
            set { this.RazinaVaznosti = value; }
        }

        public override string ToString()
        {
            return (this.tekst + "," + this.autor + "," + this.RazinaVaznosti);
        }

    }    
}

