﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarpunLv1
{
    class Program
    {
        static void Main(string[] args)
        {
            Zabiljeska novela, kupnja, kopija;

            novela = new Zabiljeska();
            kupnja = new Zabiljeska("Jaja, kruh i mlijeko", "Zena", 0);
            kopija = new Zabiljeska(novela);
            Console.WriteLine(novela.getAutor());
            Console.WriteLine(novela.getTekst());
            Console.WriteLine(kupnja.getAutor());
            Console.WriteLine(kupnja.getTekst());
            Console.WriteLine(kopija.getAutor());
            Console.WriteLine(kopija.getTekst());
            Console.WriteLine(novela.ToString());
            novela.ToString();
            Console.ReadLine();
            
        }
    }
}
